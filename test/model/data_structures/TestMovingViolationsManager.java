package model.data_structures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import model.logic.MovingViolationsManager;
import model.vo.VOMovingViolations;

public class TestMovingViolationsManager {
	
	private MovingViolationsManager manager = new MovingViolationsManager();
	
	public void setupEscenario0( )
    {

        try
        {
        
        	manager.loadMovingViolations();
   
        }
        catch( Exception e )
        {
            fail( "No deber�a generar excepci�n." );
        }

    }

	public void setupEscenario1( )
    {

        try
        {
        
        	manager.loadMovingViolations();
   
        }
        catch( Exception e )
        {
            fail( "No deber�a generar excepci�n." );
        }

    }
	
	
	
	@Test
	public void testPrimerSemestre()
	{
		setupEscenario0();
		assertEquals(599207, manager.darQueue().size());
	}
}
