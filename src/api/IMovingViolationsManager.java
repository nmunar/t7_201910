package api;

import java.time.LocalDateTime;

import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.vo.VOMovingViolations;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IMovingViolationsManager {

	/**
	 * Method to load the Moving Violations of the STS
	 * @param movingViolationsFile - path to the file 
	 */
	int loadMovingViolations();
	
	int[] darCantMes();

	 VOMovingViolations buscarInfraccion(int objectId);	
	
	RedBlackBST<Integer, VOMovingViolations> darArbol();

	void darInfoInfraccionesEnRango(int objectIdMenor, int objectIdMayor);

}
