package model.logic;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.opencsv.CSVReader;
import api.IMovingViolationsManager;
import jdk.net.NetworkPermission;
import model.vo.VOMovingViolations;
import model.data_structures.IQueue;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.util.Sort;

public class MovingViolationsManager implements IMovingViolationsManager {

	private Queue<VOMovingViolations> queueInfracciones = new Queue<VOMovingViolations>();

	private VOMovingViolations[] listaInfracciones;

	private boolean carga = false;

	private VOMovingViolations[] arreglo = new VOMovingViolations[599207];

	private int[] cantMeses = new int[6];

	private RedBlackBST<Integer, VOMovingViolations> arbol = new RedBlackBST<Integer, VOMovingViolations>();

	Scanner sc = new Scanner(System.in);

	String objectId = "";
	String location = "";
	String ticketIssueDate ="";
	String totalPaid ="";
	String accidentIndicator ="";
	String violationDescription ="";
	String sumaFINEAMT ="";
	String violationCode ="";
	String adressId = "";
	String streetSegId = "";
	String PENALTY1 =""; 
	String PENALTY2 ="";
	String fineAMT = "";
	String xCoord ="";
	String yCoord = "";

	public Queue<VOMovingViolations> darQueue() {

		return queueInfracciones;

	}

	int pos = 0;
	public int loadMovingViolations() {

		int contMes = 0;
		int cont = 0;
		carga = true;

		JsonReader reader;
		JsonParser parser = new JsonParser();

		try {

			String archJson = "./data/Moving_Violations_Issued_in_January_2018.json";

			for(int i = 0; i < 6; i++) {

				JsonArray jsonArray = (JsonArray) parser.parse(new FileReader(archJson));

				for(int j = 0; j < jsonArray.size(); j++) {


					JsonObject jsonObject = (JsonObject) jsonArray.get(j);

					objectId = jsonObject.get("OBJECTID").getAsString();
					location = jsonObject.get("LOCATION").getAsString();
					xCoord = jsonObject.get("XCOORD").getAsString();
					yCoord = jsonObject.get("YCOORD").getAsString();
					ticketIssueDate = jsonObject.get("TICKETISSUEDATE").getAsString();
					fineAMT = jsonObject.get("FINEAMT").getAsString();
					totalPaid = jsonObject.get("TOTALPAID").getAsString();
					accidentIndicator = jsonObject.get("ACCIDENTINDICATOR").getAsString();
					violationDescription = jsonObject.get("VIOLATIONDESC").getAsString();
					violationCode = jsonObject.get("VIOLATIONCODE").getAsString();
					adressId = "0";

					try {

						adressId = jsonObject.get("ADDRESS_ID").getAsString();
						streetSegId = jsonObject.get("STREETSEGID").getAsString();

					} catch (Exception e) {
						// TODO: handle exception
					}

					//Se crea el objeto VOMovingViolations

					VOMovingViolations infraccion = new VOMovingViolations(objectId, location, ticketIssueDate, totalPaid, accidentIndicator, violationDescription, adressId, fineAMT, violationCode,streetSegId,xCoord,yCoord);
					arreglo[j] = infraccion; 
					queueInfracciones.enqueue(infraccion);

					int keyA = Integer.parseInt(objectId);

					arbol.put(keyA, infraccion);

					cont++;
					contMes++;


				}

				if(i==0)
				{

					cantMeses[0] = contMes;
					contMes = 0;
					archJson = "./data/Moving_Violations_Issued_in_February_2018.json";
				}else if(i ==1)
				{
					cantMeses[1] = contMes;
					contMes = 0;
					archJson ="./data/Moving_Violations_Issued_in_March_2018.json";
				} else if(i == 2) {

					cantMeses[2] = contMes;
					contMes = 0;
					archJson = "./data/Moving_Violations_Issued_in_April_2018.json";

				} else if(i == 3) {

					cantMeses[3] = contMes;
					contMes = 0;
					archJson ="./data/Moving_Violations_Issued_in_May_2018.json";

				} else if(i == 4) {

					cantMeses[4] = contMes;
					contMes = 0;
					archJson ="./data/Moving_Violations_Issued_in_June_2018.json";

				}

				cantMeses[5] = contMes;
				contMes = 0;

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return cont;

	}


	/**
	 * Busqueda binaria de un arreglo
	 * @param arreglo Arreglo en el cual se va a buscar el dato
	 * @param dato Dato a buscar
	 * @return Dato encontrado
	 */
	public static int buscar( VOMovingViolations [] arreglo, String dato) {
		int inicio = 0;
		int fin = arreglo.length - 1;
		int pos;
		Sort.ordenarShellSort(arreglo, 8);
		while (inicio <= fin) {
			pos = (inicio+fin) / 2;
			if ( arreglo[pos].getViolationCode().compareTo(dato)==0 )
				return pos;
			else if ( arreglo[pos].getViolationCode().compareTo(dato) < 0 ) {
				inicio = pos+1;
			} else {
				fin = pos-1;
			}
		}
		return -1;
	}

	//	public  void promedio()
	//	{
	//		double sumaAMTparaPromedio=0;
	//		int contDat=0;
	//		VOMovingViolations[] aux = listaInfracciones;
	//
	//		Sort.ordenarShellSort(aux, 5);
	//		for (int i = 0; i < aux.length; i++) {
	//			contDat++;
	//			sumaAMTparaPromedio+=aux[i].getFineAmt();
	//		}
	//
	//		media= sumaAMTparaPromedio/(double)contDat;
	//
	//
	//	}


	/**
	 * Invertir una muestra de datos (in place).
	 * datos[0] y datos[N-1] se intercambian, datos[1] y datos[N-2] se intercambian, datos[2] y datos[N-3] se intercambian, ...
	 * @param datos - conjunto de datos a invertir (inicio) y conjunto de datos invertidos (final)
	 */
	public void invertirMuestra( VOMovingViolations[ ] datos ) {
		// TODO implementar

		VOMovingViolations datosAux[] = new VOMovingViolations[datos.length];

		int pos = 0;
		for(int i = datos.length-1; i >= 0; i--) {

			datosAux[pos] = datos[i];
			pos++;
		}

		datos = datosAux;

	}


	@Override
	public VOMovingViolations buscarInfraccion(int objectId) {

		return arbol.get(objectId);
	}
	
	/*@Override
	public void darInfoInfraccionesEnRango(int objectIdMenor, int objectIdMayor) {



		Iterator<VOMovingViolations> it = arbol.valuesInRange(objectIdMenor, objectIdMenor).iterator();

		VOMovingViolations informacion;

		while( it.hasNext())
		{

			informacion = (VOMovingViolations)it.next();
			System.out.println("\t"+informacion.getobjectId());
			System.out.println("ObjectId: " + informacion.getobjectId());
			System.out.println("Location: " + informacion.getLocation());
			System.out.println("AddressId: " + informacion.getAdressId());
			if(informacion.getStreetSegId().isEmpty())
			{
				System.out.println("StreedSegId: No tiene ");
			}else
			{
				System.out.println("StreedSegId: " + informacion.getStreetSegId());
			}

			System.out.println("xCoord: " + informacion.getXCoord());
			System.out.println("yCoord: " + informacion.getYCoord());
			System.out.println("TicketIssueDate: " + informacion.getTicketIssueDate());


		}



	}*/

	@Override
	public void darInfoInfraccionesEnRango(int objectIdMenor, int objectIdMayor) {

		VOMovingViolations informacion;

		if(objectIdMayor < objectIdMenor)
		{
			System.out.println("El rango est� mal dado");
		}

		for (int i = objectIdMenor; i <= objectIdMayor; i++) 
		{
			informacion = buscarInfraccion(i);

			if(informacion == null)
			{
				i++;
			}else
			{
				System.out.println("\n");

				System.out.println("ObjectId: " + informacion.getobjectId());
				System.out.println("Location: " + informacion.getLocation());
				System.out.println("AddressId: " + informacion.getAdressId());
				if(informacion.getStreetSegId().isEmpty())
				{
					System.out.println("StreedSegId: No tiene ");
				}else
				{
					System.out.println("StreedSegId: " + informacion.getStreetSegId());
				}

				System.out.println("xCoord: " + informacion.getXCoord());
				System.out.println("yCoord: " + informacion.getYCoord());
				System.out.println("TicketIssueDate: " + informacion.getTicketIssueDate());
			}
		}



	}

	@Override
	public int[] darCantMes() {
		return cantMeses;
	}

	@Override
	public RedBlackBST<Integer, VOMovingViolations> darArbol() {
		// TODO Auto-generated method stub
		return arbol;
	}

}
