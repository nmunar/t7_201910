package model.util;

import model.logic.MovingViolationsManager;
import model.vo.VOMovingViolations;

public class Sort {

	/**
	 * Metodos impelemtados de Algorithms, 4th Edition by Robert Sedgewick and Kevin Wayne
	 */

	//----------------------------------------------------------------------------------------------------------

	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	private static void merge( VOMovingViolations[ ] datos, VOMovingViolations[ ] aux, int lo,int mid ,int hi, int definidor ) 
	{
		assert isSorted(datos, lo, mid,definidor);
		assert isSorted(datos, mid+1, hi,definidor);

		// copy to aux[]
		for (int k = lo; k <= hi; k++) 
		{
			aux[k] = datos[k]; 
		}			

		// merge back to a[]
		int i = lo, j = mid+1;
		for (int k = lo; k <= hi; k++) {
			if      (i > mid)              datos[k] = aux[j++];
			else if (j > hi)               datos[k] = aux[i++];
			else if (less(aux[j], aux[i],definidor)) datos[k] = aux[j++];
			else                           datos[k] = aux[i++];
		}
		// postcondition: a[lo .. hi] is sorted
		assert isSorted(datos, lo, hi,definidor);
	}


	public static void ordenarMergeSort(VOMovingViolations[] a, VOMovingViolations[] aux, int lo, int hi, int definidor) 
	{
		if (hi <= lo) return;
		int mid = lo + (hi - lo) / 2;
		ordenarMergeSort(a, aux, lo, mid,definidor);
		ordenarMergeSort(a, aux, mid + 1, hi,definidor);
		merge(a, aux, lo, mid, hi,definidor);
	}
	private static void odenarMergeSort(VOMovingViolations[] a,int definidor) 
	{
		VOMovingViolations[] aux = new VOMovingViolations[a.length];
		ordenarMergeSort(a, aux, 0, a.length-1,definidor);
		assert isSorted(a, definidor);
	}

	private static boolean isSorted(VOMovingViolations[] a,int def) {
		return isSorted(a, 0, a.length - 1,def);
	}

	private static boolean isSorted(VOMovingViolations[] a, int lo, int hi,int def) {
		for (int i = lo + 1; i <= hi; i++)
			if (less(a[i], a[i-1],def)) return false;
		return true;
	}


	//----------------------------------------------------------------------------------------------------------

	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(VOMovingViolations v, VOMovingViolations w,int def)
	{
		if(def==1)
		{
			return (v.compareTo(w) < 0);
		}else if(def==2)
		{
			return (v.compareFecha(w) < 0);
		}	else if(def ==4)
		{
			return (v.compareViolationDesc(w)< 0);
		}else if(def == 5)
		{
			return (v.compareFINEAMT(w)< 0);
		}else if(def==6)
		{
			return (v.compareTOTALPAID(w)<0);
		}else if(def==7)
		{
			return (v.compareHour(w)<0);
		}else
		{
			return (v.compareViolationCode(w)<0);
		}
	}

	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		// DONE implementar
		Comparable swap = datos[i];
		datos[i] = datos[j];
		datos[j] = swap;
	}

	//------------------------------------------------------------------------------------------------------------
	public static void ordenarShellSort( VOMovingViolations[ ] datos, int def ) {

		// DONE implementar el algoritmo ShellSort
		int n = datos.length;
		// 3x+1 increment sequence:  1, 4, 13, 40, 121, 364, 1093, ... 
		int h = 1;
		while (h < n/3) h = 3*h + 1; 

		while (h >= 1) {
			// h-sort the array
			for (int i = h; i < n; i++) {
				for (int j = i; j >= h && less(datos[j], datos[j-h],def); j -= h) {
					exchange(datos, j, j-h);
				}
			}
			h /= 3;
		}
	}

	public static void ordenarShellSortINV( VOMovingViolations[ ] datos, int def ) {

		// DONE implementar el algoritmo ShellSort
		int n = datos.length;
		// 3x+1 increment sequence:  1, 4, 13, 40, 121, 364, 1093, ... 
		int h = 1;
		while (h < n/3) h = 3*h + 1; 

		while (h >= 1) {
			// h-sort the array
			for (int i = h; i < n; i++) {
				for (int j = i; j >= h && lessINV(datos[j], datos[j-h],def); j -= h) {
					exchange(datos, j, j-h);
				}
			}
			h /= 3;
		}
	}

	private static boolean lessINV(VOMovingViolations v, VOMovingViolations w,int def)
	{return (v.compareFecha(w) > 0);

	}


}
