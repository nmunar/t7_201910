package view;

import model.data_structures.IQueue;
import model.data_structures.RedBlackBST;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	public MovingViolationsManagerView() 
	{

	}

	public void printMenu() {

		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 7----------------------");
		System.out.println("1. Cargar datos del primer semestre");
		System.out.println("2. Consultar información asociada a un ObjectId de la infración");
		System.out.println("3. Consultar los ObjectIds de las infracciones registradas en un rango");
		System.out.println("4.Salir");		

		System.out.println("Digite el número de opción para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}

	public void printMessage(String mensaje) {
		System.out.println(mensaje);
	}


}
