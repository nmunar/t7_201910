package controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Scanner;

import api.IMovingViolationsManager;
import model.data_structures.IQueue;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.logic.MovingViolationsManager;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {

	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;

	private static IMovingViolationsManager  manager = new MovingViolationsManager();

	private int option2 = 0;

	private boolean carga = false;


	public Controller() {
		view = new MovingViolationsManagerView();

		//TODO, inicializar la pila y la cola
		movingViolationsQueue = null;
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:

				if(carga == false) {
					
					carga = true;

					int cantTotal = this.loadMovingViolations();
					
					System.out.println("Infracciones totales por mes: " +  cantTotal);
					
					int[] cantMes = this.darCantMes();
					
					String[] meses = {"Enero", "Febrero", "Marzo", "Abril" , "Mayo", "Junio"};
					
					System.out.println("Las cantidades por cada mes fueron:");
					
					for(int i = 0; i < cantMes.length; i++) {
						
						System.out.println(meses[i] + " : " + cantMes[i]);
						
					}

				} else {

					System.out.println("No se permite cargar los archivos m�s de una vez.");
					System.out.println("");

				}
				break;

			case 2:

				if(carga == true) {

					int obId;

					System.out.println("Digite el ObjectId");

					obId = sc.nextInt();
					
					VOMovingViolations informacion = buscarIfracciones(obId);
					
					System.out.println("Location: " + informacion.getLocation());
					System.out.println("AddressId: " + informacion.getAdressId());
					if(informacion.getStreetSegId().isEmpty())
					{
						System.out.println("StreedSegId: No tiene ");
					}else
					{
						System.out.println("StreedSegId: " + informacion.getStreetSegId());
					}
					
					System.out.println("xCoord: " + informacion.getXCoord());
					System.out.println("yCoord: " + informacion.getYCoord());
					System.out.println("TicketIssueDate: " + informacion.getTicketIssueDate());


				}
				else {
					System.out.println("Debe primero cargar un semestre.");
				}

				break;

			case 3:

				if(carga == true) {
					int obIdMayor;
					int obIdMenor;

					System.out.println("Digite el ObjectId menor");

					obIdMenor = sc.nextInt();

					System.out.println("Digite el ObjectId mayor");

					obIdMayor = sc.nextInt();

					darInfraccionesEnRango(obIdMenor, obIdMayor);

				} else {

					System.out.println("Debe primero cargar un semestre.");

				}
				break;

			case 4:	

				fin=true;
				sc.close();
				break;
				
			
			case 5:
				
				
				System.out.println("Total nodos: " + this.darArbol().size());
				System.out.println("Altura: " + this.darArbol().height());
				
				int suma = 0;
				int cantKeys = 0;
				
				Iterator<Integer> it = manager.darArbol().keys().iterator();
				
				while(it.hasNext()) {
					
					int key = it.next();
					int height = manager.darArbol().getNHeight(key);
					suma+= height;
					cantKeys++;
					
				}
				
				double promedio = suma/cantKeys;
				System.out.println("Promedio de altura: " + promedio);
			
			}
		}

	}

	public void darInfraccionesEnRango(int obIdMenor,  int obIdMayor)
	{
		manager.darInfoInfraccionesEnRango(obIdMenor, obIdMayor);
	}

	public int loadMovingViolations() {

		return manager.loadMovingViolations();

	}
	
	
	public int[] darCantMes() {
	
		return manager.darCantMes();
		
	}
	

	
	public RedBlackBST<Integer, VOMovingViolations> darArbol() {
		
		return manager.darArbol();
		
	}

	private   VOMovingViolations buscarIfracciones(int objectId) {
		return manager.buscarInfraccion(objectId);
	}


}
